CREATE SCHEMA `DEMO` ;


CREATE TABLE `DEMO`.`member` (
  `memid` INT NOT NULL,
  `name` VARCHAR(45) NULL,
  `tel` VARCHAR(45) NULL,
  PRIMARY KEY (`memid`));
