var express = require('express');
var mysql = require('mysql');
var redis = require("ioredis");
var format = require("string-template");

var db_option = {
	connectionLimit : 10,
    host: 'localhost',		//'db4free.net',
    user: 'sayyo',			//'spark999',
    password: 'sayyogames',	//'spark0220',
    database: 'DEMO',		//'spark_test999',
	port: 3306
};

var redis_option = {
	port: 15890,          // Redis port
	host: 'redis-15890.c12.us-east-1-4.ec2.cloud.redislabs.com',   // Redis host
	family: 4,           // 4 (IPv4) or 6 (IPv6)
	password: '2Ko4CgZMQiP11rpZWo5Zn7o6ESt4eWfB',
	db: 0
};

var app = express();
var pool = mysql.createPool(db_option);
var cache = new redis() //new redis(redis_option);

//var sltSqltmp = "SELECT memid, name, tel FROM DEMO WHERE memid = '{memid}';";
//var insSqltmp = "INSERT INTO DEMO (memid, name, tel) VALUES ('{memid}', '{name}', '{tel}');";
//var delSqltmp = "DELETE FROM DEMO WHERE memid = '{memid}';";
//var updtSqltmp = "UPDATE DEMO SET name = '{name}', tel = '{tel}' WHERE memid = '{memid}';";

var sltSqltmp = "SELECT memid, name, tel FROM MEMBER WHERE memid = '{memid}';";
var insSqltmp = "INSERT INTO MEMBER (memid, name, tel) VALUES ('{memid}', '{name}', '{tel}');";
var delSqltmp = "DELETE FROM MEMBER WHERE memid = '{memid}';";
var updtSqltmp = "UPDATE MEMBER SET name = '{name}', tel = '{tel}' WHERE memid = '{memid}';";

var sltExec = function(tranPack){
	return new Promise(function(res, rej){
		if(Boolean(tranPack.iscache) == false){
			var sqlCmd = format(sltSqltmp, tranPack.data);
			console.log(sqlCmd);
			tranPack.con.query(sqlCmd, function (error, results, fields) {
				if(error){
					tranPack.con.release();
					rej(error);
				}
				else
				{
					if(results.length != 0)
					{
						tranPack.data.name = results[0].name;
						tranPack.data.tel = results[0].tel;
						cache.hmset(tranPack.data.memid, 'name', tranPack.data.name, 'tel', tranPack.data.tel);
						cache.expire(tranPack.data.memid, 60);
					}
					res(tranPack);
				}
			});
		}
		else{
			res(tranPack);
		}
	})
};

var insExec = function(tranPack){
	return new Promise(function(res, rej){
		var sqlCmd = format(insSqltmp, tranPack.data);
		console.log(sqlCmd);
		tranPack.con.query(sqlCmd, function (error, results, fields) {
			if(error){
				tranPack.con.release();
				rej(error);
			}
			else
			{
				res(tranPack);
			}
		});
	})
};

var delExec = function(tranPack){
	return new Promise(function(res, rej){
		var sqlCmd = format(delSqltmp, tranPack.data);
		console.log(sqlCmd);
		tranPack.con.query(sqlCmd, function (error, results, fields) {
			if(error){
				tranPack.con.release();
				rej(error);
			}
			else
			{
				res(tranPack);
			}
		});
	})
};

var updateExec = function(tranPack){
	return new Promise(function(res, rej){
		var sqlCmd = format(updtSqltmp, tranPack.data);
		console.log(sqlCmd);
		tranPack.con.query(sqlCmd, function (error, results, fields) {
			if(error){
				tranPack.con.release();
				rej(error);
			}
			else
			{
				res(tranPack);
			}
		});
	})
};

var relDB = function(tranPack){
	return new Promise(function(res, rej){
		console.log("rel");
		if(Boolean(tranPack.iscache) == false){
			tranPack.con.release();
		}
		res(tranPack);
	});
};

var conDB = function(tranPack){
	return new Promise(function(res, rej){
		if(Boolean(tranPack.iscache) == false)
		{
			pool.getConnection(function(error, connection) {
				tranPack.con = connection;
				if(error)
				{
					connection.end();
					rej(error);
				}
				else 
				{
					res(tranPack);
				}
			});
		}
		else
		{
			res(tranPack);
		}
	});
};

var queryCache = function(tranPack){
	return new Promise(function(res, rej){
		cache.hgetall(tranPack.data.memid, function(err, rel){
			if(err) {
				rej(error);
			}
			else{
				if(Boolean(rel.name) == true && Boolean(rel.tel) == true)
				{
					tranPack.data.name = rel.name;
					tranPack.data.tel = rel.tel;
					tranPack.iscache = true;
					cache.expire(tranPack.data.memid, 60);
				}
				res(tranPack);
			}
		});
	});
};

app.use(express.json());

app.post('/insert', function(request, response) {
	console.log('post insert');
	console.log(request.body);
	if(Boolean(request.body.memid) == false || Boolean(request.body.name) == false || Boolean(request.body.tel) == false)
		response.send('DATA ERROR');
	else
	{
		var tranPack = {con: undefined, data: request.body};
		conDB(tranPack)
		.then(insExec)
		.then(relDB)
		.then(tp=>{console.log(tp.data);response.json(tp.data);})
		.catch(err=>{response.send('inser error');});
	}
});

app.post('/delete', function(request, response) {
	if(Boolean(request.body.memid) == false)
		response.send('DATA ERROR');
	else
	{
		var tranPack = {con: undefined, data: request.body};
		conDB(tranPack)
		.then(delExec)
		.then(relDB)
		.then(tp=>{console.log(tp.data);response.json(tp.data);})
		.catch(err=>{console.log(err)});
	}
});

app.post('/update', function(request, response) {
	if(Boolean(request.body.memid) == false || Boolean(request.body.update.name) == false || Boolean(request.body.update.tel) == false)
		response.send('DATA ERROR');
	else
	{
		var tranPack = {con: undefined, data: {memid: request.body.memid, name: request.body.update.name, tel: request.body.update.tel}};
		conDB(tranPack)
		.then(updateExec)
		.then(relDB)
		.then(tp=>{console.log(tp.data);response.json(tp.data);})
		.catch(err=>{console.log(err)});
	}
});

app.post('/query', function(request, response) {
	if(Boolean(request.body.memid) == false)
		response.send('DATA ERROR');
	else
	{
		var tranPack = {con: undefined, data: request.body};
		queryCache(tranPack)
		.then(conDB)
		.then(sltExec)
		.then(relDB)
		.then(tp=>{console.log(tp.data);response.json(tp.data);})
		.catch(err=>{console.log(err)});
	}
});

var port = 3000;//process.env.PORT || 5000;
app.listen(port, function() {
  console.log("Listening on " + port);
});